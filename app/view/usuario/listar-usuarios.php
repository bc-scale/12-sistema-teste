<?php include __DIR__ . '/../inicio-html.php'; ?>

<div class="container-fluid p-0 d-flex justify-content-between">
    <span>
        <h1><?= $titulo; ?></h1>
    </span>
    <span>
        <button type="button" class="btn btn-primary" onclick="document.location.href='/novo-usuario'">Novo</button>
        <button type="button" class="btn btn-secondary" onclick="document.location.href='/home'">Fechar</button>
    </span>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Descrição</th>
            <th scope="col" class="min-vw-25 d-flex justify-content-end">Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($usuarios as $usuario) { ?>
            <tr>
                <td><?= $usuario->getNome(); ?></td>
                <td class="min-vw-25 d-flex justify-content-end">
                    <a href="/consultar-usuario?id=<?= $usuario->getId(); ?>" class="table-link">
                        <span class="visually-hidden">Consultar usuário <?= $usuario->getNome(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-search"></i>
                        </span>
                    </a>
                    <a href="/alterar-usuario?id=<?= $usuario->getId(); ?>" class="table-link">
                        <span class="visually-hidden">Alterar usuário <?= $usuario->getNome(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-pencil-square"></i>
                        </span>
                    </a>
                    <a href="/excluir-usuario?id=<?= $usuario->getId(); ?>" class="table-link danger">
                        <span class="visually-hidden">Excluir usuário <?= $usuario->getNome(); ?></span>
                        <span class="fa-stack">
                            <i class="bi bi-trash"></i>
                        </span>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php include __DIR__ . '/../fim-html.php'; ?>