<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Lotacao
{
    /**
     * @Id
     * @GeneratedValue
     * @Column (type="integer")
     */
    private $id;
    /**
     * @Column (type="string")
     */
    private $sigla;
    /**
     * @Column (type="string")
     */
    private $nome;
    /**
     * @ManyToOne(targetEntity="Cidade")
     */
    private $cidade;
    /**
     * @OneToMany (targetEntity="Funcionario", mappedBy="lotacao", cascade={"persist"})
     */
    private $funcionarios;

    public function __construct()
    {
        $this->funcionarios = new ArrayCollection();
    }

    public function addFuncionario(Funcionario $funcionario): self
    {
        if ($this->funcionarios->contains($funcionario)) {
            return $this;
        }

        $this->funcionarios->add($funcionario);
        $funcionario->setLotacao($this);
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSigla(): string
    {
        return $this->sigla;
    }

    public function setSigla(string $sigla): self
    {
        $this->sigla = $sigla;
        return $this;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;
        return $this;
    }

    public function getCidade(): Cidade
    {
        return $this->cidade;
    }

    public function setCidade(Cidade $cidade): self
    {
        if($this->cidade == $cidade) {
            return $this;
        }

        $this->cidade = $cidade;
        $cidade->addLotacao($this);
        return $this;
    }
}