<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Cargo 
{
    /**
     * @Id
     * @GeneratedValue
     * @Column (type="integer")
     */
    private $id;
    /**
     * @Column (type="string")
     */
    private $descricao;
    /**
     * @Column (type="integer")
     */
    private $carga_horaria;
    /**
     * @Column (type="integer")
     */
    private $dias_semana;
    /**
     * @OneToMany (targetEntity="Funcionario", mappedBy="cargo", cascade={"persist"})
     */
    private $funcionarios;

    public function __construct()
    {
        $this->funcionarios = new ArrayCollection();
    }

    public function addFuncionario(Funcionario $funcionario): self
    {
        if ($this->funcionarios->contains($funcionario)) {
            return $this;
        }

        $this->funcionarios->add($funcionario);
        $funcionario->setCargo($this);
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    public function setDescricao(string $descricao): self
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getCargaHoraria(): ?int
    {
        return $this->carga_horaria;
    }

    public function setCargaHoraria(int $cargaHoraria): self
    {
        $this->carga_horaria = $cargaHoraria;
        return $this;
    }

    public function getDiasSemana(): ?int
    {
        return $this->dias_semana;
    }

    public function setDiasSemana(int $diasSemana): self
    {
        $this->dias_semana = $diasSemana;
        return $this;
    }
}