<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity
 */
class Funcionario 
{
    /**
     * @Id
     * @GeneratedValue
     * @Column (type="integer")
     */
    private $id;
    /**
     * @Column (type="string")
     */
    private $nome;
    /**
     * @Column (type="string", nullable=true)
     */
    private $login;
    /**
     * @Column (type="string", nullable=true)
     */
    private $senha;
    /**
     * @Column (type="integer", nullable=true)
     */
    private $matricula;
    /**
     * @Column (type="time", nullable=true)
     */
    private $horario_inicio;
    /**
     * @Column (type="time", nullable=true)
     */
    private $horario_fim;
    /**
     * @ManyToOne (targetEntity="Cargo", inversedBy="funcionarios")
     */
    private $cargo;
    /**
     * @ManyToOne (targetEntity="Lotacao", inversedBy="funcionarios")
     */
    private $lotacao;
    /**
     * @ManyToMany (targetEntity="Menu", inversedBy="autorizados")
     * @JoinTable(
     *      name="Autorizacao", 
     *      joinColumns={@JoinColumn(name="funcionario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="menu_id", referencedColumnName="id")}
     * )
     */
    private $autorizacoes;

    public function __construct()
    {
        $this->autorizacoes = new ArrayCollection();
    }

    public function __toString(): string
    {
        $horarioInicio = $this->horario_inicio->format('H:i');
        $horarioFim = $this->horario_fim->format('H:i');
        $cargo = $this->cargo->getDescricao();
        $lotacao = $this->lotacao->getNome();


        $res = "
            id: $this->id
            nome: $this->nome
            matrícula: $this->matricula
            horário início: $horarioInicio
            horário fim: $horarioFim
            cargo: $cargo
            lotação: $lotacao
        ";
        return $res;
    }

    public function addAutorizacao(Menu $menu): self
    {
        $menu->addAutorizado($this);
        
        $this->autorizacoes->add($menu);
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getAutorizacoes(): ?Collection
    {
        return $this->autorizacoes;
    }

    public function setCargo(Cargo $cargo): self
    {
        if($this->cargo == $cargo) {
            $res = clone $this;
            unset($res->senha);
            return $res;
        }

        $this->cargo = $cargo;
        $cargo->addFuncionario($this);
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getCargo(): ?Cargo
    {
        return $this->cargo;
    }

    public function setLotacao(Lotacao $lotacao): self
    {
        if($this->lotacao == $lotacao) {
            $res = clone $this;
            unset($res->senha);
            return $res;
        }

        $this->lotacao = $lotacao;
        $lotacao->addFuncionario($this);
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getLotacao(): ?Lotacao
    {
        return $this->lotacao;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function setSenha(string $senha)
    {
        $this->senha = password_hash($senha, PASSWORD_ARGON2I);
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function verificaSenha(string $senhaPura): bool
    {
        return password_verify($senhaPura, $this->senha);
    }

    public function getMatricula(): ?int
    {
        return $this->matricula;
    }

    public function setMatricula(int $matricula): self
    {
        $this->matricula = $matricula;
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getHorarioInicio(): ?\DateTime
    {
        return $this->horario_inicio;
    }

    public function setHorarioInicio(\DateTime $horarioInicio): self
    {
        $this->horario_inicio = $horarioInicio;
        $res = clone $this;
        unset($res->senha);
        return $res;
    }

    public function getHorarioFim(): ?\DateTime
    {
        return $this->horario_fim;
    }

    public function setHorarioFim(\DateTime $horarioFim): self
    {
        $this->horario_fim = $horarioFim;
        $res = clone $this;
        unset($res->senha);
        return $res;
    }
}