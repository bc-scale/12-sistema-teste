<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126141140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Lotacao (id INT AUTO_INCREMENT NOT NULL, cidade_id INT DEFAULT NULL, sigla VARCHAR(255) NOT NULL, nome VARCHAR(255) NOT NULL, INDEX IDX_83D7F8C39586CC8 (cidade_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Lotacao ADD CONSTRAINT FK_83D7F8C39586CC8 FOREIGN KEY (cidade_id) REFERENCES Cidade (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE Lotacao');
    }
}
