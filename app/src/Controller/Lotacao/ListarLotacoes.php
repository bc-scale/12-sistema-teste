<?php

namespace App\Controller\Lotacao;

use App\Entity\Lotacao;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListarLotacoes implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    private $entityManager;
    private $repositorioDeLotacoes;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeLotacoes = $entityManager->getRepository(Lotacao::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $lotacoes = $this->repositorioDeLotacoes->findAll();

        $html = $this->renderizaHtml('lotacao/listar-lotacoes.php', [
            'titulo' => 'Lotações',
            'lotacoes' => $lotacoes
        ]);

        return new Response(100, [], $html);
    }
}