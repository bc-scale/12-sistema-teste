<?php

namespace App\Controller\Usuario;

use App\Entity\Funcionario;
use App\Entity\Menu;
use App\Helper\FlashMessageTrait;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FormularioConsultarUsuario implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeUsuarios;
    private $repositorioDeMenus;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeUsuarios = $entityManager->getRepository(Funcionario::class);
        $this->repositorioDeMenus = $entityManager->getRepository(Menu::class);
    }
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Usuário inválido.');
            return new Response(302, ['Location' => '/listar-usuarios']);
        }

        $usuario = $this->repositorioDeUsuarios->find($idEntidade);
        $idAutorizacoes = $usuario->getAutorizacoes()->map(function($item) {
            return $item->getId();
        });
        $cargos = [];
        array_push($cargos, $usuario->getCargo());
        $lotacoes = [];
        array_push($lotacoes, $usuario->getLotacao());

        $menus = $this->repositorioDeMenus->findAll();


        $html = $this->renderizaHtml('/usuario/formulario.php', [
            'titulo' => 'Consultar usuário',
            'usuario' => $usuario,
            'cargos' => $cargos,
            'lotacoes' => $lotacoes,
            'idAutorizacoes' => $idAutorizacoes,
            'menus' => $menus,
            'formDisabled' => true,
            'senhaPlaceholder' => "&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;"
        ]);

        return new Response(200, [], $html);
    }
}