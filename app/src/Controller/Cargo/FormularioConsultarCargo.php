<?php

namespace App\Controller\Cargo;

use App\Entity\Cargo;
use App\Entity\Funcionario;
use App\Entity\Menu;
use App\Helper\FlashMessageTrait;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FormularioConsultarCargo implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeCargos;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeCargos = $entityManager->getRepository(Cargo::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Cargo inválido.');
            return new Response(302, ['Location' => '/listar-cargos']);
        }

        $cargo = $this->repositorioDeCargos->find($idEntidade);

        $html = $this->renderizaHtml('/cargo/formulario.php', [
            'titulo' => 'Consultar cargo',
            'cargo' => $cargo,
            'formDisabled' => true
        ]);

        return new Response(200, [], $html);
    }
}